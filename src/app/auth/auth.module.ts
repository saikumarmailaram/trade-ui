import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { routes } from './auth-routing.module';
import { RegisterComponent } from './register/register.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [LoginComponent, RegisterComponent, ResetPasswordComponent],
})
export class AuthModule { }
