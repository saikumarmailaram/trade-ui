import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RolesComponent } from './roles/roles.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  { path : 'roles', component : RolesComponent},
  { path : 'users', component : UsersComponent},
  { path : '**', component : RolesComponent},
];

@NgModule({
  declarations: [RolesComponent, UsersComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
})
export class AdminModule { }
