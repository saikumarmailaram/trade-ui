import { Routes } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { UsersComponent } from './users/users.component';
import { RequestListComponent } from './request-list/request-list.component';
import { RequestDetailsComponent } from './request-details/request-details.component';

export const routes: Routes = [
  { path: '', component: SearchComponent},
  { path: 'users', component: UsersComponent },
  { path: 'request-list', component: RequestListComponent },
  { path: 'request-details', component: RequestDetailsComponent },
  { path: '**', component: SearchComponent},
];
