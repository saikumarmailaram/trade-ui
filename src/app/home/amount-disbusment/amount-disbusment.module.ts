import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { routes } from './amount-disbusment-routing.module';
import { SearchComponent } from './search/search.component';
import { UsersComponent } from './users/users.component';
import { RequestListComponent } from './request-list/request-list.component';
import { RequestDetailsComponent } from './request-details/request-details.component';

@NgModule({
  declarations: [SearchComponent, UsersComponent, RequestListComponent, RequestDetailsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
})
export class AmountDisbusmentModule { }
