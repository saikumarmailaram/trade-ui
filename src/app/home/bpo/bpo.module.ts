import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { routes } from './bpo-routing.module';
import { RouterModule } from '@angular/router';
import { InboundComponent } from './inbound/inbound.component';
import { OutboundComponent } from './outbound/outbound.component';
import { AssignCsmComponent } from './assign-csm/assign-csm.component';
import { CouponStatusComponent } from './coupon-status/coupon-status.component';
import { LookupUserComponent } from './lookup-user/lookup-user.component';
import { NgxPermissionsModule } from 'ngx-permissions';

@NgModule({
  declarations: [
    InboundComponent,
    OutboundComponent,
    AssignCsmComponent,
    CouponStatusComponent,
    LookupUserComponent,
    ],
  imports: [
    CommonModule,
    NgxPermissionsModule.forChild(),
    RouterModule.forChild(routes),
  ],
})
export class BpoModule { }
