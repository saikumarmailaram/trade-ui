import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { NgxPermissionsGuard, NgxPermissionsModule } from 'ngx-permissions';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['admin'],
        redirectTo: '/auth',
      },
    },
  },
  {
    path: '**',
    component: DashboardComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['admin'],
        redirectTo: '/auth',
      },
    },
  },
];
@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    NgxPermissionsModule.forChild(),
    RouterModule.forChild(routes),
  ],
})
export class DashboardModule { }
