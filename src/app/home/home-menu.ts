import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'shopping-cart-outline',
    link: '/home/dashboard',
    home: true,
  }, {
    title: 'Rewards',
    icon: 'layout-outline',
    children: [
      {
        title: 'Disburse Rewards',
        link: '/home/rewards',
      },
    ],
  },
  {
    title: 'Roles',
    icon: 'shopping-cart-outline',
    link: '/home/admin/roles',
  },
  {
    title: 'Users',
    icon: 'shopping-cart-outline',
    link: '/home/admin/users',
  },
  {
    title: 'Amount Disbusment',
    icon: 'home-outline',
    link: '/home/amount-disbusment',
  },
  {
    title: 'Schemes',
    icon: 'layout-outline',
    children: [
      {
        title: 'List',
        link: '/home/schemes',
      },
      {
        title: 'Skus',
        link: '/home/schemes/skus',
      },
    ],
  },
  {
    title: 'Product Catalog',
    icon: 'layout-outline',
    children: [
      {
        title: 'Skus',
        link: '/home/product-catalog',
      },
      {
        title: 'Product Group',
        link: '/home/product-catalog/product-group',
      },
      {
        title: 'Products',
        link: '/home/product-catalog/products',
      },
    ],
  },
  {
    title: 'Quality Check',
    icon: 'layout-outline',
    children: [
      {
        title: 'To Be Qualified',
        link: '/home/quality-check',
        data: ['admin', 'bpo-admin'],
      },
      {
        title: 'Qualified',
        link: '/home/quality-check/qualified',
        data: ['admin', 'bpo-admin'],
      },
      {
        title: 'Disqualified',
        link: '/home/quality-check/disqualified',
        data: ['admin', 'bpo-admin'],
      },
    ],
  },
  {
    title: 'BPO',
    icon: 'layout-outline',
    children: [
      {
        title: 'Inbound',
        link: '/home/bpo',
        data: ['admin', 'bpo-admin', 'bpo-agent'],
      },
      {
        title: 'Outbound',
        link: '/home/bpo/outbound',
        data: ['admin', 'bpo-admin', 'bpo-agent'],
      },
      {
        title: 'Assign CSM',
        link: '/home/bpo/assign-csm',
        data: ['admin', 'bpo-admin'],
      },
      {
        title: 'Coupon Status',
        link: '/home/bpo/coupon-status',
        data: ['admin', 'bpo-admin', 'bpo-agent'],
      },
      {
        title: 'Lookup User',
        link: '/home/bpo/lookup-user',
        data: ['admin', 'bpo-admin', 'bpo-agent'],
      },
    ],
  },
];

export const BPO_ADMIN: NbMenuItem[] = [
  {
    title: 'Quality Check',
    icon: 'layout-outline',
    children: [
      {
        title: 'To Be Qualified',
        link: '/home/quality-check',
      },
      {
        title: 'Qualified',
        link: '/home/quality-check/qualified',
      },
      {
        title: 'Disqualified',
        link: '/home/quality-check/disqualified',
      },
    ],
  },
  {
    title: 'BPO',
    icon: 'layout-outline',
    children: [
      {
        title: 'Inbound',
        link: '/home/bpo',
      },
      {
        title: 'Outbound',
        link: '/home/bpo/outbound',
      },
      {
        title: 'Assign CSM',
        link: '/home/bpo/assign-csm',
      },
      {
        title: 'Coupon Status',
        link: '/home/bpo/coupon-status',
      },
      {
        title: 'Lookup User',
        link: '/home/bpo/lookup-user',
      },
    ],
  },
];

export const BPO_AGENT: NbMenuItem[] = [
  {
    title: 'BPO',
    icon: 'layout-outline',
    children: [
      {
        title: 'Inbound',
        link: '/home/bpo',
      },
      {
        title: 'Outbound',
        link: '/home/bpo/outbound',
      },
      {
        title: 'Coupon Status',
        link: '/home/bpo/coupon-status',
      },
      {
        title: 'Lookup User',
        link: '/home/bpo/lookup-user',
      },
    ],
  },
];

export const FINANCE_MENU: NbMenuItem[] = [
  {
    title: 'Rewards',
    icon: 'layout-outline',
    children: [
      {
        title: 'Requests',
        link: '/home/rewards/requests',
      },
    ],
  },
];
