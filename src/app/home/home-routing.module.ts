import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { NgxPermissionsGuard } from 'ngx-permissions';


const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'schemes',
      loadChildren: () => import('./schemes/schemes.module')
        .then(m => m.SchemesModule),
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          only: ['admin'],
        },
      },
    },
    {
      path: 'product-catalog',
      loadChildren: () => import('./product-catalog/product-catalog.module')
        .then(m => m.ProductCatalogModule),
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          only: ['admin'],
        },
      },
    },
    {
      path: 'amount-disbusment',
      loadChildren: () => import('./amount-disbusment/amount-disbusment.module')
        .then(m => m.AmountDisbusmentModule),
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          only: ['admin'],
        },
      },
    },
    {
      path: 'bpo',
      loadChildren: () => import('./bpo/bpo.module')
        .then(m => m.BpoModule),
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          only: ['admin', 'bpo-admin', 'bpo-agent'],
        },
      },
    },
    {
      path: 'quality-check',
      loadChildren: () => import('./quality-check/quality-check.module')
        .then(m => m.QualityCheckModule),
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          only: ['admin', 'bpo-admin'],
        },
      },
    },
    {
      path: 'admin',
      loadChildren: () => import('./admin/admin.module')
        .then(m => m.AdminModule),
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          only: ['admin'],
        },
      },
    },
    {
      path: 'dashboard',
      loadChildren: () => import('./dashboard/dashboard.module')
        .then(m => m.DashboardModule),
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          only: ['admin'],
        },
      },
    },
    {
      path: 'rewards',
      loadChildren: () => import('./rewards/rewards.module')
        .then(m => m.RewardsModule),
      canActivate: [NgxPermissionsGuard],
      data: {
        permissions: {
          only: ['admin', 'finance'],
        },
      },
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {
}
