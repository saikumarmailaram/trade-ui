import { Routes } from '@angular/router';
import { QualifiedComponent } from './qualified/qualified.component';
import { DisqualifiedComponent } from './disqualified/disqualified.component';
import { ToBeQualifiedComponent } from './to-be-qualified/to-be-qualified.component';
import { NgxPermissionsGuard } from 'ngx-permissions';

export const routes: Routes = [
  {
    path: '',
    component: ToBeQualifiedComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['admin', 'bpo-admin'],
        redirectTo: '/auth',
      },
    },
  },
  {
    path: 'qualified',
    component: QualifiedComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['admin', 'bpo-admin'],
        redirectTo: '/auth',
      },
    },
  },
  {
    path: 'disqualified',
    component: DisqualifiedComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['admin', 'bpo-admin'],
        redirectTo: '/auth',
      },
    },
  },
  {
    path: '**',
    component: ToBeQualifiedComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['admin', 'bpo-admin'],
        redirectTo: '/auth',
      },
    },
  },
];

