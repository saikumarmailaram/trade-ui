import { Routes } from '@angular/router';
import { RequestsComponent } from './requests/requests.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { DisburseRewardsComponent } from './disburse-rewards/disburse-rewards.component';

export const routes: Routes = [
  {
    path: '',
    component: DisburseRewardsComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['admin'],
        redirectTo: '/auth',
      },
    },
  },
  {
    path: 'requests',
    component: RequestsComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['admin', 'finance'],
        redirectTo: '/auth',
      },
    },
  },
];
