import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisburseRewardsComponent } from './disburse-rewards/disburse-rewards.component';
import { RequestsComponent } from './requests/requests.component';
import { NgxPermissionsModule } from 'ngx-permissions';
import { RouterModule } from '@angular/router';
import { routes } from './rewards-routing.module';
@NgModule({
  declarations: [DisburseRewardsComponent, RequestsComponent],
  imports: [
    CommonModule,
    NgxPermissionsModule.forChild(),
    RouterModule.forChild(routes),
  ],
})
export class RewardsModule { }
