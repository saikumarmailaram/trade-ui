import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ListSchemesComponent } from './list-schemes/list-schemes.component';
import { SkusListComponent } from './rewards/skus-list.component';
import { NgxPermissionsGuard, NgxPermissionsModule } from 'ngx-permissions';


export const routes: Routes = [
  {
    path: '',
    component: ListSchemesComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['admin'],
      },
    },
  },
  {
    path: 'skus',
    component: SkusListComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['admin'],
      },
    },
  },
];
@NgModule({
  declarations: [ListSchemesComponent, SkusListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgxPermissionsModule.forChild(),
  ],
})
export class SchemesModule { }
